This repository is automatically mirrored to the public at [ska-low-csiro-args](https://gitlab.com/ska-telescope/low-cbf/ska-low-csiro-args).

## Project Avatar (Repository Icon)
[Treasure icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/treasure "treasure icons")
